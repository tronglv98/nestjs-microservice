CREATE DATABASE IF NOT EXISTS demo_user;
CREATE DATABASE IF NOT EXISTS demo_access_user;

CREATE USER 'demo_user'@'%' IDENTIFIED BY 'demo@user1';
GRANT ALL ON demo_user.* TO 'demo_user'@'%';

CREATE USER 'demo_access_user'@'%' IDENTIFIED BY 'demo@access_user1';
GRANT ALL ON demo_access_user.* TO 'demo_access_user'@'%';