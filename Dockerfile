FROM node:12.14-alpine
COPY ./package.json /app/
EXPOSE 3000 50051
WORKDIR /app
RUN npm install -g typescript
RUN npm install -g ts-node
RUN npm install -g ts-node-dev

RUN npm install -g @nestjs/cli
RUN npm install

COPY . /app
RUN npm run build

CMD ["npm", "run", "start:dev"]