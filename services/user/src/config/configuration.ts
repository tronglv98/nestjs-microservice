export default () => ({
  database: {
    type: process.env.DATABASE_TYPE || 'mysql',
    host: process.env.DATABASE_HOST || 'db',
    port: parseInt(process.env.DATABASE_PORT, 10) || 3306,
    username: process.env.DATABASE_USERNAME || 'demo_user',
    password: process.env.DATABASE_PASSWORD || 'demo@user1',
    database: process.env.DATABASE_NAME || 'demo_user',
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: true,

    // test: {
    //   type: process.env.DATABASE_TYPE || 'mysql',
    //   host: process.env.DATABASE_HOST || 'db',
    //   port: parseInt(process.env.DATABASE_PORT, 10) || 3306,
    //   username: process.env.DATABASE_USERNAME_TEST || 'root',
    //   password: process.env.DATABASE_PASSWORD_TEST || 'root',
    //   database: process.env.DATABASE_NAME_TEST || 'test_demo',
    // },
  },
  cache: {
    host: process.env.REDIS_HOST || 'localhost',
    port: parseInt(process.env.REDIS_PORT, 10) || 6379,
  },
});
