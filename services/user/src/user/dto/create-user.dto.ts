import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  Length,
  Matches,
} from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(10, 100)
  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  @Length(5, 200)
  name: string;

  @IsString()
  @IsNotEmpty()
  @Length(10, 255)
  @Matches('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
  password: string;

  @IsString()
  @Length(0, 255)
  @IsOptional()
  avatarUrl = null;

  @IsBoolean()
  @IsOptional()
  isActive?: boolean = true;
}
