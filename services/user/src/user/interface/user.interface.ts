import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';

export interface UserById {
  id: string;
}

// export interface UserResponse {
//   id: string;
//   name: string;
//   email: string;
//   password: string;
//   avatarUrl: string;
//   isActive: boolean;
//   createdAt: Date;
//   updatedAt: Date;
// }

// export interface UserRequest {
//   name: string;
//   email: string;
//   password: string;
//   avatarUrl: string;
//   isActive: boolean;
// }

export interface UserController {
  FindOne(request: UserById): Promise<User>;
  Create(request: CreateUserDto): Promise<User>;
}
