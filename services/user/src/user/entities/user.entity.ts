import { Exclude } from 'class-transformer';
import { IsEmail, Length } from 'class-validator';
import * as bcrypt from 'bcrypt';
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public id!: string;

  @Column({ type: 'varchar', unique: true, width: 100 })
  @IsEmail()
  @Length(10, 100)
  public email!: string;

  @Exclude()
  @BeforeInsert()
  @BeforeUpdate()
  hashPassword() {
    if (!!this.password) {
      this.password = bcrypt.hashSync(this.password, 10);
    }
  }
  @Column({ type: 'varchar', width: 255 })
  @Length(0, 255)
  password!: string;

  @Column({ type: 'varchar', width: 100 })
  @Length(10, 100)
  public name!: string;

  @Column({
    type: 'varchar',
    width: 255,
    nullable: true,
    default: null,
    name: 'avatar_url',
  })
  @Length(0, 255)
  public avatarUrl!: string;

  @Column({ type: 'boolean', default: true, name: 'is_active' })
  @Length(10, 100)
  isActive!: boolean;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
