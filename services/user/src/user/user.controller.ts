import { Metadata, ServerUnaryCall } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
// import { ExceptionFilter } from 'src/filter/rpc-exception.filter';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { UserById } from './interface/user.interface';
import { UserService } from './service/user/user.service';
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}
  @GrpcMethod('UserController', 'FindOne')
  findOne(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    data: UserById,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    metaData: Metadata,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    call: ServerUnaryCall<any, any>,
  ): User {
    return {
      email: 'trongle@gmail.com',
    } as User;
  }

  // @UseFilters(new ExceptionFilter())
  @GrpcMethod('UserController', 'Create')
  async create(
    data: CreateUserDto,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    metaData: Metadata,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    call: ServerUnaryCall<any, any>,
  ): Promise<User> {
    const user: User = await this.userService.create(data);
    return user;
  }
}
