import { Metadata, ServerUnaryCall } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { AuthRequest, AuthResponse } from './interface/auth.interface';

@Controller('auth')
export class AuthController {
  @GrpcMethod('AuthController', 'Login')
  login(
    data: AuthRequest,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    metaData: Metadata,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    call: ServerUnaryCall<any, any>,
  ): AuthResponse {
    const user = { name: new Date() };
    return user;
  }
}
