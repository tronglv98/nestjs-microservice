export interface AuthResponse {
  name: Date;
}

export interface AuthRequest {
  username: string;
  password: string;
}

export interface AuthController {
  Login(request: AuthRequest): Promise<AuthResponse>;
}
