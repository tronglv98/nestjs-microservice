import { HttpAdapterHost, NestFactory } from '@nestjs/core';
// import { MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { AllExceptionsFilter } from './filter/all-exceptions.filter';
import { HttpExceptionFilter } from './filter/http-exception.filter';
import { ExceptionFilter } from './filter/rpc-exception.filter';
// import { grpcClientOptions } from './grpc-client.options';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.connectMicroservice<MicroserviceOptions>(grpcClientOptions);
  // app.startAllMicroservices();
  const httpAdapter = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionsFilter(httpAdapter));
  app.useGlobalFilters(new ExceptionFilter());
  app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(3000);
}
bootstrap();
