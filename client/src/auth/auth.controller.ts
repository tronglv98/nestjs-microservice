import { Body, Controller, Inject, Post } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import {
  AuthRequest,
  AuthResponse,
  AuthService,
} from './interfaces/auth.interface';

@Controller('auth')
export class AuthController {
  private authService: AuthService;

  constructor(@Inject('AUTH_PACKAGE') private readonly client: ClientGrpc) {}

  onModuleInit() {
    this.authService = this.client.getService<AuthService>('AuthController');
  }

  @Post('login')
  create(@Body() data: AuthRequest): Observable<AuthResponse> {
    return this.authService.Login(data);
  }
}
