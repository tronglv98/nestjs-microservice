import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AuthController } from './auth.controller';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'AUTH_PACKAGE',
        transport: Transport.GRPC,
        options: {
          url: 'auth_service:50051',
          package: ['auth'],
          protoPath: [join(__dirname, '/../../proto/auth.proto')],
        },
      },
    ]),
  ],
  controllers: [AuthController],
})
export class AuthModule {}
