import { Observable } from 'rxjs';

export interface AuthService {
  Login(data: AuthRequest): Observable<AuthResponse>;
}
export interface AuthResponse {
  name: string;
}

export interface AuthRequest {
  username: string;
  password: string;
}
