import { Observable } from 'rxjs';

export interface UserService {
  FindOne(data: UserById): Observable<UserResponse>;
  Create(data: UserRequest): Observable<UserResponse>;
}

export interface UserById {
  id: number;
}

export class UserResponse {
  id: number;
  name: string;
  email: string;
  password: string;
  avatarUrl: string = null;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface UserRequest {
  name: string;
  email: string;
  password: string;
  avatarUrl: string;
  isActive: boolean;
}
