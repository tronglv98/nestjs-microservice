import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
// import { grpcClientOptions } from 'src/grpc-client.options';
import { UserController } from './user.controller';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'USER_PACKAGE',
        transport: Transport.GRPC,
        options: {
          url: 'user_service:50051',
          package: ['user'],
          protoPath: [join(__dirname, '/../../proto/user.proto')],
        },
      },
    ]),
  ],
  controllers: [UserController],
})
export class UserModule {}
