import {
  Body,
  Controller,
  Get,
  Inject,
  OnModuleInit,
  Param,
  Post,
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { of, from } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  UserResponse,
  UserRequest,
  UserService,
} from './interfaces/user.interface';

@Controller('user')
export class UserController implements OnModuleInit {
  private userService: UserService;

  constructor(@Inject('USER_PACKAGE') private readonly client: ClientGrpc) {}

  onModuleInit() {
    this.userService = this.client.getService<UserService>('UserController');
  }

  @Get(':id')
  getById(@Param('id') id: string): Observable<UserResponse> {
    return this.userService.FindOne({ id: +id });
  }
  @Post()
  async create(@Body() data: UserRequest): Promise<Observable<UserResponse>> {
    const a = this.userService.Create(data);

    return await this.userService.Create(data).pipe(
      map((data) => {
        const c = { ...data } as UserResponse;
        console.log(typeof c.createdAt);
        return data;
      }),
    );
  }
}
