import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const grpcClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: '0.0.0.0:50051',
    package: ['user', 'auth'],
    protoPath: [
      join(__dirname, '../../proto/user.proto'),
      join(__dirname, '../../proto/auth.proto'),
    ],
  },
};
